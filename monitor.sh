

#! /bin/sh

#***********************************************#
#                                               #
#  MONITOR - Unirede Solucoes Corporativas   #
#  ------------------------------------------   #
#  Recebe ate n parametros e retorna um valor   #
#  Ex.: monitor.sh <funcao> <par1> <par2> <...> #
#                                               #
#***********************************************#
#
# 01/09/2010 - Alterada versao para 1.0 (inclusao monitoramento baseline seguranca lojas).
#
# Debug
# set -x

# Versao do 'monitor'
# Utilizada para validar as versoes distribuidas
version="1.0"
export LANG=pt_BR

rval=0

case $1 in
'versao')
        echo "$version"
        ;;

'salt')
var=`sudo cat /etc/salt/minion_id`
host=`uname -n`
        case `uname -n` in
                LOR) SERV=`echo L001`;;
                TOR) SERV=`echo T001`;;
                LPB) SERV=`echo L002`;;
                TPB) SERV=`echo T002`;;
                LJP) SERV=`echo L003`;;
                TJP) SERV=`echo T003`;;
                LFL) SERV=`echo L004`;;
                TFL) SERV=`echo T004`;;
                LPL) SERV=`echo L005`;;
                TPL) SERV=`echo T005`;;
                LJV) SERV=`echo L006`;;
                TJV) SERV=`echo T006`;;
                LRG) SERV=`echo L007`;;
                TRG) SERV=`echo T007`;;
                LCC) SERV=`echo L008`;;
                TCC) SERV=`echo T008`;;
                LCB) SERV=`echo L009`;;
                TCB) SERV=`echo T009`;;
                LNH) SERV=`echo L010`;;
                TNH) SERV=`echo T010`;;
                LSM) SERV=`echo L011`;;
                TSM) SERV=`echo T011`;;
                LCX) SERV=`echo L012`;;
                TCX) SERV=`echo T012`;;
                LIG) SERV=`echo L013`;;
                TIG) SERV=`echo T013`;;
                *) SERV=`uname -n`;;
        esac

if [ -e /etc/salt/minion_id ]
   then
      if [ ${var} == ${SERV} ]
         then
            echo 0
         else
            echo 2
      fi
   else
      echo 1
fi
;;

'offline_pdv')
        LOJA=`uname -n`

        TAM=`ls -s /u/pdv/linux/upgrade/PDV_BATCH.tar.gz 2> /dev/null | cut -d" " -f1`
        DAT=`cat /u/pdv/linux/upgrade/PDV_BATCH 2> /dev/null`
        set -- "$TAM" "$DAT"

        monitoracao="`date`"
        if [ "$1" -a "$2" ]
        then
                TEM=`date +%Y%m%d%H%M%S`
                if [ "$1" -gt 2000 -a `expr $TEM - $2` -lt 240000 ]
                then
                        monitoracao="`date` ' --- OK ---'"
                        echo "Geracao: OK"
                else
                echo "Geracao Atrasada: `echo $DAT|cut -c7-8`.`echo $DAT|cut -c5-6`.`echo $DAT|cut -c1-4` `echo $DAT|cut -c9-10`:`echo $DAT|cut -c11-12`:`echo $DAT|cut -c13-14`"
                fi
        fi
        ;;
'area_bco_mysql')
        SERV=`uname -n`

LINHA=`/u/mysql/bin/mysql -updv -ppdv1997 -h$SERV autm 2>&1 <<EOF | tail -1
show table status like "APPROVED_PRICE";
EOF
`

        set -- $LINHA
        [ "$1" = "ERROR" ] && exit 1

        set -- `echo $LINHA | grep -o -e "free:.*"`

        echo $2
        ;;
'cluster')
        IPCLUSTER=`( sudo /bin/grep ltsp /etc/dhcp/dhcpd.conf 2> /dev/null || sudo /bin/grep ltsp /etc/dhcpd.conf 2> /dev/null ) | head -1 | cut -d: -f1 | cut -d\" -f2`
        /sbin/ip a     |grep -c ${IPCLUSTER}
        ;;

'stonith')
        sudo tail -50 /var/log/messages | grep "stonith-ng" | wc -l
        ;;

'epserver')
        sudo grep -c "epserver" /etc/hosts
        ;;

'spserver')
        SP=`sudo grep -c "spserver" /etc/hosts`
        IP=`sudo grep -c -w $2 /etc/hosts`
        echo ${SP} + ${IP} | bc
        ;;

'ilouser')
        ps aux | grep "ipmitool lan print" | awk '{print $2}' | xargs kill 2>/dev/null
        channel=`for i in \`seq 1 14\`; do sudo ipmitool lan print $i 2>/dev/null | grep -q ^Set && echo $i; done`
        sudo ipmitool user list $channel | grep lojasrenner | wc -l
        ;;

'dns_dhcp')
DOMAIN=`sudo grep "option domain-name-servers 10.1.5.100,10.1.4.100,10.20.4.100;" /etc/dhcp/dhcpd.conf`
if [ "${DOMAIN}" = 'option domain-name-servers 10.1.5.100,10.1.4.100,10.20.4.100;' ]
        then
                echo "1"
        else
                echo "0"
fi
;;

'garantia')
export http_proxy=http://cache.lojasrenner.com.br:3128
export https_proxy=https://cache.lojasrenner.com.br:3128


Serial=`sudo lshw -quiet | grep "serial:" | head -1 | cut -d: -f2 | sed 's/ //g'`

wget -nv -q -O /tmp/warranty "https://support.hpe.com/hpsc/wc/public/find?rows%5B0%5D.item.serialNumber=${Serial}&submitButton=Submit"
Date=`grep "HP Collaborative Remote Support"  /tmp/warranty | sed 's/>/>\n/g' | grep "</td>" | tail -2 | head -1 | cut -d"<" -f1`


date -d "${Date} 00:00:00 EDT" +%s
;;

'home_user')
IP=`sudo grep "option root-path" /etc/dhcp/dhcpd.conf | head -1 | cut -d\" -f2 | cut -d: -f1`
IP_up=`ip a | grep -c ${IP}`
PING=`fping ${IP} 2>/dev/null | awk '{print $3}'`
USER=`sudo df -h | grep -c "/home/user"`

if [ ${IP_up} -eq 1 ]
   then
      if [ ${PING} = 'alive' ]
         then
            if [ ${USER} -ge 1 ]
               then
#                  echo "ip no servidor, ip vivo e user ativo"
                  echo 1
               else
#                  echo "ip no servidor, ip vivo e user nao ativo"
                  echo 0
            fi
         else
#            echo "sem no servidor, ip nao esta vivo"
            echo 1
      fi
   else
#      echo "sem ip no servidor"
      echo 1
fi
           ;;

'standby')
sudo pcs status | grep standby | grep `uname -n` | wc -l
;;

'pcs_fail')
#PCS=`sudo pcs status | grep -c Failed`
#if [ ${PCS} -eq 1 ]
#   then
#      sudo crm_resource --resource ClusterIP --cleanup
#      sudo crm_resource --resource HomeFS --cleanup
#      sudo crm_resource --resource zabbix_proxy --cleanup
#fi

sudo pcs status | grep -c Failed
;;

'dns_ltsp')
LTSP5=`sudo grep "ltsp" /etc/dhcp/dhcpd.conf | grep -c "/5/"`
if [ ${LTSP5} -ge 1 ]
        then
                DNS5=`sudo grep "DNS_SERVER" /opt/ltsp/5/i386/etc/lts.conf | cut -d= -f2 | sed 's/^ //g'`
                if [ "${DNS5}" = '10.1.5.100 10.1.4.100' ]
                        then
                                echo "1"
                        else
                                echo "0"
                fi
        else
                DNS4=`sudo grep "DNS_SERVER" /opt/ltsp/4/i386/etc/lts.conf | cut -d= -f2 | sed 's/^ //g'`
                if [ "${DNS4}" = '10.1.5.100 10.1.4.100' ]
                        then
                                echo "1"
                        else
                                echo "0"
                fi
fi
;;

'ping107')
        case `uname -n` in
                LOR) SERV=`echo L001`;;
                TOR) SERV=`echo L001`;;
                LPB) SERV=`echo L002`;;
                TPB) SERV=`echo L002`;;
                LJP) SERV=`echo L003`;;
                TJP) SERV=`echo L003`;;
                LFL) SERV=`echo L004`;;
                TFL) SERV=`echo L004`;;
                LPL) SERV=`echo L005`;;
                TPL) SERV=`echo L005`;;
                LJV) SERV=`echo L006`;;
                TJV) SERV=`echo L006`;;
                LRG) SERV=`echo L007`;;
                TRG) SERV=`echo L007`;;
                LCC) SERV=`echo L008`;;
                TCC) SERV=`echo L008`;;
                LCB) SERV=`echo L009`;;
                TCB) SERV=`echo L009`;;
                LNH) SERV=`echo L010`;;
                TNH) SERV=`echo L010`;;
                LSM) SERV=`echo L011`;;
                TSM) SERV=`echo L011`;;
                LCX) SERV=`echo L012`;;
                TCX) SERV=`echo L012`;;
                LIG) SERV=`echo L013`;;
                TIG) SERV=`echo L013`;;
                *) SERV=`uname -n`;;
        esac
        SERV2=`sudo grep "option root-path" /etc/dhcp/dhcpd.conf | head -1 | cut -d\" -f2 | cut -d: -f1`
        IP=`host $SERV | awk '{print $4}' | cut -d. -f1-3`
        PING=`fping $IP.107 2>/dev/null | awk '{print $3}'`
        if [ $PING = alive ]
                then
                        echo 1
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP01' -k "ping107" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP32' -k "ping107" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP33' -k "ping107" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP34' -k "ping107" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP35' -k "ping107" -o 1 2>&1 >/dev/null
                else
                        echo 0
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP01' -k "ping107" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP32' -k "ping107" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP33' -k "ping107" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP34' -k "ping107" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP35' -k "ping107" -o 0 2>&1 >/dev/null
        fi
        ;;

'ping108')
        case `uname -n` in
                LOR) SERV=`echo L001`;;
                TOR) SERV=`echo L001`;;
                LPB) SERV=`echo L002`;;
                TPB) SERV=`echo L002`;;
                LJP) SERV=`echo L003`;;
                TJP) SERV=`echo L003`;;
                LFL) SERV=`echo L004`;;
                TFL) SERV=`echo L004`;;
                LPL) SERV=`echo L005`;;
                TPL) SERV=`echo L005`;;
                LJV) SERV=`echo L006`;;
                TJV) SERV=`echo L006`;;
                LRG) SERV=`echo L007`;;
                TRG) SERV=`echo L007`;;
                LCC) SERV=`echo L008`;;
                TCC) SERV=`echo L008`;;
                LCB) SERV=`echo L009`;;
                TCB) SERV=`echo L009`;;
                LNH) SERV=`echo L010`;;
                TNH) SERV=`echo L010`;;
                LSM) SERV=`echo L011`;;
                TSM) SERV=`echo L011`;;
                LCX) SERV=`echo L012`;;
                TCX) SERV=`echo L012`;;
                LIG) SERV=`echo L013`;;
                TIG) SERV=`echo L013`;;
                *) SERV=`uname -n`;;
        esac
        SERV2=`sudo grep "option root-path" /etc/dhcp/dhcpd.conf | head -1 | cut -d\" -f2 | cut -d: -f1`
        IP=`host $SERV | awk '{print $4}' | cut -d. -f1-3`
        PING=`fping $IP.108 2>/dev/null | awk '{print $3}'`
        if [ $PING = alive ]
                then
                        echo 1
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP01' -k "ping108" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP32' -k "ping108" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP33' -k "ping108" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP34' -k "ping108" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP35' -k "ping108" -o 1 2>&1 >/dev/null
                else
                        echo 0
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP01' -k "ping108" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP32' -k "ping108" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP33' -k "ping108" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP34' -k "ping108" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP35' -k "ping108" -o 0 2>&1 >/dev/null
        fi
        ;;


'ping109')
        case `uname -n` in
                LOR) SERV=`echo L001`;;
                TOR) SERV=`echo L001`;;
                LPB) SERV=`echo L002`;;
                TPB) SERV=`echo L002`;;
                LJP) SERV=`echo L003`;;
                TJP) SERV=`echo L003`;;
                LFL) SERV=`echo L004`;;
                TFL) SERV=`echo L004`;;
                LPL) SERV=`echo L005`;;
                TPL) SERV=`echo L005`;;
                LJV) SERV=`echo L006`;;
                TJV) SERV=`echo L006`;;
                LRG) SERV=`echo L007`;;
                TRG) SERV=`echo L007`;;
                LCC) SERV=`echo L008`;;
                TCC) SERV=`echo L008`;;
                LCB) SERV=`echo L009`;;
                TCB) SERV=`echo L009`;;
                LNH) SERV=`echo L010`;;
                TNH) SERV=`echo L010`;;
                LSM) SERV=`echo L011`;;
                TSM) SERV=`echo L011`;;
                LCX) SERV=`echo L012`;;
                TCX) SERV=`echo L012`;;
                LIG) SERV=`echo L013`;;
                TIG) SERV=`echo L013`;;
                *) SERV=`uname -n`;;
        esac
        SERV2=`sudo grep "option root-path" /etc/dhcp/dhcpd.conf | head -1 | cut -d\" -f2 | cut -d: -f1`
        IP=`host $SERV | awk '{print $4}' | cut -d. -f1-3`
        PING=`fping $IP.109 2>/dev/null | awk '{print $3}'`
        if [ $PING = alive ]
                then
                        echo 1
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP01' -k "ping109" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP32' -k "ping109" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP33' -k "ping109" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP34' -k "ping109" -o 1 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP35' -k "ping109" -o 1 2>&1 >/dev/null
                else
                        echo 0
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP01' -k "ping109" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP32' -k "ping109" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP33' -k "ping109" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP34' -k "ping109" -o 0 2>&1 >/dev/null
                        sudo /usr/local/sbin/zabbix_sender -s LOJA_${SERV} -z 'SP35' -k "ping109" -o 0 2>&1 >/dev/null
        fi
        ;;

'mysql_sync')
# Verifica se a tabela cads_fill dos dois servidores estao com as mesmas informacoes
L=`uname -n | sed 's/^T/L/g'`
COUNT_L=`echo -e "select count(*) from cads_fill;" | /u/mysql/bin/mysql -updv -ppdv1997 -h${L} -AsN autm`
COUNT_T=`echo -e "select count(*) from cads_fill;" | /u/mysql/bin/mysql -updv -ppdv1997 -hlocalhost -AsN autm`

if [ ${COUNT_T} -eq ${COUNT_L} ]
        then
                # MYSQL OK
                echo 1
        else
                # MYSQL NOK
                echo 0
fi
;;

'mysql_consist')
# Executa um show tables status para verificar se as tabelas estão consistentes
echo -e "show table status;" | /u/mysql/bin/mysql -updv -ppdv1997 -AsN autm | grep -c "Incorrect information"
;;


'printer_job') var=`lpstat -o | wc -l`
if [ $var -ge 5 ]
   then
      echo "$var"
      sudo /usr/bin/cancel -a
   else
      echo $var
fi
;;

'wrapper') var=`sudo ls -ltrh /usr/p2ksp/sp_lj9999/AtualizadorVersaoSp/bin/wrapper.log | awk '{print $3}' | grep -c p2ksp`

if [ $var -ne 1 ]
   then
      echo $var
      sudo chown p2ksp:p2ksp /usr/p2ksp/sp_lj9999/AtualizadorVersaoSp/bin/wrapper.log*
   else
      echo $var
fi
;;

'max_memory') sudo dmidecode -t 16 | grep Max | awk '{print $3}';;

'slot_memory') sudo dmidecode -t 16 | grep Number | awk '{print $4}';;

'slot_free') sudo dmidecode -t memory | grep -i size | grep -i "no module" | wc -l;;

'serial_hardware')
                 if [ `cat /etc/redhat-release | grep -c "release 6"` = "1" ]
                    then
                       lshal | grep system.hardware.serial | awk '{print $3}' | cut -d"'" -f2
                    else
                       sudo lshw | grep "serial:" | cut -d: -f2 | sed 's/^ //g'| head -1
                 fi
                ;;

'printers')
LOJA=`uname -n | sed 's/^[TL]//g' | sed 's/^0//g' | tr '[A-Za-z]' '[a-za-z]'`
IMP=`sudo cat /etc/cups/printers.conf | grep "Info il${LOJA}" | wc -l`
echo ${IMP}
;;

'timezone')

        case `uname -n` in
                LOR) LOJA=`echo L001`;;
                TOR) LOJA=`echo T001`;;
                LPB) LOJA=`echo L002`;;
                TPB) LOJA=`echo T002`;;
                LJP) LOJA=`echo L003`;;
                TJP) LOJA=`echo T003`;;
                LFL) LOJA=`echo L004`;;
                TFL) LOJA=`echo T004`;;
                LPL) LOJA=`echo L005`;;
                TPL) LOJA=`echo T005`;;
                LJV) LOJA=`echo L006`;;
                TJV) LOJA=`echo T006`;;
                LRG) LOJA=`echo L007`;;
                TRG) LOJA=`echo T007`;;
                LCC) LOJA=`echo L008`;;
                TCC) LOJA=`echo T008`;;
                LCB) LOJA=`echo L009`;;
                TCB) LOJA=`echo T009`;;
                LNH) LOJA=`echo L010`;;
                TNH) LOJA=`echo T010`;;
                LSM) LOJA=`echo L011`;;
                TSM) LOJA=`echo T011`;;
                LCX) LOJA=`echo L012`;;
                TCX) LOJA=`echo T012`;;
                LIG) LOJA=`echo L013`;;
                TIG) LOJA=`echo T013`;;
                *) LOJA=`uname -n`;;
        esac

        UF=`cat /u/pdv/linux/p2k/LOJA | grep P2K_UF | cut -d= -f2`
        [ -f /etc/localtime ] && continue ||  cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

        case $UF in
                DF|ES|GO|MG|PR|RJ|RS|SC|SP) VAR=`cksum /usr/share/zoneinfo/America/Sao_Paulo | awk '{print $1}'`
                TIME=`echo Sao_Paulo`
                ;;

                AL|AP|BA|CE|MA|PA|PB|PE|PI|RN|SE|TO) VAR=`cksum /usr/share/zoneinfo/America/Bahia | awk '{print $1}'`
                TIME=`echo Bahia`
                ;;

                MS) VAR=`cksum /usr/share/zoneinfo/America/Campo_Grande | awk '{print $1}'`
                TIME=`echo Campo_Grande`
                ;;

                MT) VAR=`cksum /usr/share/zoneinfo/America/Cuiaba | awk '{print $1}'`
                TIME=`echo Cuiaba`
                ;;

                RR|AM|RO) VAR=`cksum /usr/share/zoneinfo/America/Manaus | awk '{print $1}'`
                TIME=`echo Manaus`
                ;;

                AC) VAR=`cksum /usr/share/zoneinfo/America/Bogota | awk '{print $1}'`
                TIME=`echo Bogota`
                ;;

                *) echo "0" ;;
        esac

        if [ "$VAR" -ne `cksum /etc/localtime | awk '{print $1}'` ]
                then
                        rsync -va /usr/share/zoneinfo/America/${TIME} /etc/localtime >/dev/null 2>&1
        fi
        if [ "$VAR" -ne `cksum /u/pdv/linux/install/localtime | awk '{print $1}'` ]
                then
                        rsync -va /usr/share/zoneinfo/America/${TIME} /u/pdv/linux/install/localtime >/dev/null 2>&1
        fi

        CK=`cksum /etc/localtime | awk '{print $1}'`
        CK1=`cksum /u/pdv/linux/install/localtime | awk '{print $1}'`

        if [ "$VAR" -eq "$CK" ] && [ "$VAR" -eq "$CK1" ]
                then
                        echo 1
                else
                        echo 0
        fi
;;

'ip_servidor')
                VERSION=`sed 's/release /release |/g' /etc/redhat-release | cut -d'|' -f2 | cut -d. -f1`
                case $VERSION in
                '6') VAR=`host \`uname -n\` | awk '{print $4}' | cut -d. -f4`
                     IP=`host \`uname -n\` | awk '{print $4}' | cut -d. -f1-3`
                     case $VAR in
                        '101') IP_T=`echo ${IP}.100`
                               IP_L=`echo ${IP}.101`
                             ;;
                        '100') IP_T=`echo ${IP}.100`
                               IP_L=`echo ${IP}.101`
                               ;;
                        '12') IP_T=`echo ${IP}.11`
                              IP_L=`echo ${IP}.12`
                            ;;
                        '11') IP_T=`echo ${IP}.11`
                              IP_L=`echo ${IP}.12`
                            ;;
                         *) echo "ZBX_NOTSUPPORTED"
                            ;;
                     esac
                     MAC_T=`/sbin/ifconfig | /bin/grep -B 1 $IP_T | /usr/bin/head -1 | /bin/awk '{print $7}'`
                     MAC_L=`/sbin/ifconfig | /bin/grep -B 1 $IP_L | /usr/bin/head -1 | /bin/awk '{print $7}'`

                     if [ -z $MAC_T ]
                           then
                              MAC_T=`echo 0`
                     fi

                     if [ -z $MAC_L ]
                           then
                              MAC_L=`echo 0`
                     fi

                     if [ $MAC_T = $MAC_L ]
                        then
                           echo "1"
                        else
                           echo "0"
                     fi
                   ;;
                '7') VAR=`host \`uname -n\` | awk '{print $4}' | cut -d. -f4`
                     IP=`host \`uname -n\` | awk '{print $4}' | cut -d. -f1-3`
                     case $VAR in
                        '101') IP_T=`echo ${IP}.100`
                               IP_L=`echo ${IP}.101`
                             ;;
                        '100') IP_T=`echo ${IP}.100`
                               IP_L=`echo ${IP}.101`
                               ;;
                        '12') IP_T=`echo ${IP}.11`
                              IP_L=`echo ${IP}.12`
                            ;;
                        '11') IP_T=`echo ${IP}.11`
                              IP_L=`echo ${IP}.12`
                            ;;
                         *) echo "ZBX_NOTSUPPORTED"
                            ;;
                     esac
                     MAC_T=`/sbin/ifconfig | /bin/grep -A 2 $IP_T | /usr/bin/tail -1 | /bin/awk '{print $2}'`
                     MAC_L=`/sbin/ifconfig | /bin/grep -A 2 $IP_L | /usr/bin/tail -1 | /bin/awk '{print $2}'`

                     if [ -z $MAC_T ]
                           then
                              MAC_T=`echo 0`
                     fi

                     if [ -z $MAC_L ]
                           then
                              MAC_L=`echo 0`
                     fi

                     if [ $MAC_T = $MAC_L ]
                        then
                           echo "1"
                        else
                           echo "0"
                     fi
                   ;;
                 *) echo "ZBX_NOTSUPPORTED";;
esac
;;



'imp')
        unset LANG
        imp=`lpstat -t | grep disabled | awk '{print $2}' | wc -l`

        if [ ${imp} -eq 0 ]
        then
           echo "0"
        else
           imp_dis=`lpstat -t | grep disabled | awk '{print $2}'`
           echo $imp_dis
           sudo /usr/sbin/cupsreject $imp_dis
           sudo /usr/sbin/cupsaccept $imp_dis
           sudo /usr/sbin/cupsenable $imp_dis
        fi
        ;;

'imp2')
        unset LANG
        imp=`lpstat -t | grep disabled | awk '{print $2}' | wc -l`

        if [ ${imp} -eq 0 ]
        then
           echo "0"
        else
           imp_dis=`lpstat -t | grep disabled | awk '{print $2}'`
           echo $imp_dis
           sudo /usr/sbin/cupsreject $imp_dis
           sudo /usr/sbin/cupsaccept $imp_dis
           sudo /usr/sbin/cupsenable $imp_dis
        fi
        ;;

'ip_cluster')
        ipcluster=`( sudo /bin/grep ltsp /etc/dhcp/dhcpd.conf 2> /dev/null || sudo /bin/grep ltsp /etc/dhcpd.conf 2> /dev/null ) | head -1 | cut -d: -f1 | cut -d\" -f2`
        ip=`echo $ipcluster | cut -d. -f1-3`

        case $2 in
        'T')
        clusterT=`sudo /usr/bin/ssh -o stricthostkeychecking=no ${ip}.100 "/sbin/ip a |grep -c ${ipcluster}" 2>/dev/null`
        echo "${clusterT}"
        ;;
        'L')
        clusterL=`sudo /usr/bin/ssh -o stricthostkeychecking=no ${ip}.101 "/sbin/ip a |grep -c ${ipcluster}" 2>/dev/null`
        echo "${clusterL}"
        ;;
        *)
        echo ""
        ;;
        esac
;;


controller|disks)
                MOD=$(sudo dmidecode -s system-manufacturer)
                if [ "$MOD" = "HP" -o "$MOD" = "HPE" ]
                then
                    #MOD1=$(sudo dmidecode -s "system-product-name")
                    #if [ $(echo "${MOD1}" | grep -c "ProLiant DL20") -gt 0 ]
                    #then
                    #   if [ $(sudo ipmitool sdr elist | grep -i "drive present" | grep -ci "fault\|critical") -gt 0 ]
                    #   then
                    #      echo 1
                    #   else
                    #      echo 0
                    #   fi
                    # else
                       sudo sh /etc/zabbix/scripts/hpcheck_ssa.sh -d
                    #fi
                else
                    echo 0
                fi
;;

'work_PPF')
find /work/pdfimg -maxdepth 1 -mtime +1 -type f | wc -l
;;

'work_RH')
find /work/RH -maxdepth 1 -mtime +1 -type f | wc -l
;;

'p2k_dados')
ARQ=`cat /u/pdv/linux/p2k/LOJA`
LOJA=`echo ${ARQ} | cut -d= -f2 | cut -d" " -f1`
LOJA_C=`echo ${ARQ} | cut -d= -f2 | cut -d" " -f1 | wc -L`
UF=`echo ${ARQ} | cut -d= -f3 | cut -d" " -f1`
CNPJ=`echo ${ARQ} | cut -d= -f4 | cut -d" " -f1`
HOST=`uname -n`

        case `echo ${HOST}` in
                LOR) LOJ=`echo L001`;;
                LPB) LOJ=`echo L002`;;
                LJP) LOJ=`echo L003`;;
                LFL) LOJ=`echo L004`;;
                LPL) LOJ=`echo L005`;;
                LJV) LOJ=`echo L006`;;
                LRG) LOJ=`echo L007`;;
                LCC) LOJ=`echo L008`;;
                LCB) LOJ=`echo L009`;;
                LNH) LOJ=`echo L010`;;
                LSM) LOJ=`echo L011`;;
                LCX) LOJ=`echo L012`;;
                LIG) LOJ=`echo L013`;;
                *) LOJ=`uname -n`;;
        esac
LOJ=`echo ${LOJ} | sed 's/^L//g'`
if [ ${LOJ} -gt 700 ]
  then
    LOJ=`echo "A${LOJ}"`
  else
    LOJ=`echo "L${LOJ}"`
fi
QUERY=`/u/mysql/bin/mysql -hsp04.lojasrenner.com.br -udesig -prenner95 desig  -se "select loja,estado,cnpj from lojas where loja like '${LOJ}'"`
LOJA_Q=`echo $QUERY | cut -d" " -f1`
UF_Q=`echo $QUERY | cut -d" " -f2`
CNPJ_Q=`echo $QUERY | cut -d" " -f3 | sed 's/\.//g' | sed 's/\///g' | sed 's/-//g'`

case `echo ${LOJA_C}` in
        1) LOJA=`echo L00${LOJA}`;;
        2) LOJA=`echo L0${LOJA}`;;
        3) LOJA=`echo L${LOJA}`;;
        *) echo erro;;
esac

#if [ $LOJA_C -eq 2 ]
#       then
#               LOJA=`echo L0${LOJA}`
#       else
#               LOJA=`echo L${LOJA}`
#fi

if [ $LOJA = $LOJA_Q ]
        then
                if [ $UF = $UF_Q ]
                        then
                                echo 1
                        else
                                echo 0
                fi
        else
                echo 0
fi
;;

'pdv_aberto') for i in `ls /usr/p2ksp/sp_lj9999/bin/componentes/componente*`
do
cat $i | head -2 | tail -1
done >/tmp/pdv_aberto

VAR=`cat /tmp/pdv_aberto | sort -n | uniq -c | grep --text -E '(RECEB_CREDITO_DIGITAL|RECEB_PAGAMENTO_CONTA|CONSULTA_DISPONIVEL|DISPONIVEL|CANCELAMENTO_QUALQUER_ITEM|RECEBIMENTO|VENDA|CONSULTA_VENDA)' | awk '{print $1}' | tr '\n' '+' | sed 's/+$//' | sed 's/+/ + /g'`

if [ -z "${VAR}" ]
        then
                VAR=`echo "0"`
                expr $VAR
        else
                expr $VAR
fi
;;

'pdv_fechado') for i in `ls /usr/p2ksp/sp_lj9999/bin/componentes/componente*`
do
cat $i | head -2 | tail -1
done >/tmp/pdv_fechado

VAR=`cat /tmp/pdv_fechado | sort -n | uniq -c | grep -E '(FECHADO|FECHADO_PARCIAL|PAUSA)' | awk '{print $1}' | tr '\n' '+' | sed 's/+$//' | sed 's/+/ + /g'`
expr $VAR
;;

'hostname') uname -n ;;

'eth1_con') sudo ethtool eth1 | tail -1 | grep -c yes
;;

'eth1_link') VAR=`sudo ethtool eth1 | grep detected | awk '{print $3}'`
        case $VAR in
                no) echo 0;;
                yes) echo 1;;
                *) echo 3;;
        esac
;;

'temp.amb')
                #sudo ipmitool sensor | grep "Inlet Ambient" | cut -d"|" -f2 | awk '{print $1}' | cut -d. -f1
                temp=$(sudo ipmitool sensor get_temp_reading "01-Inlet Ambient" 2> /dev/null| grep "Sensor Reading" | cut -d"|" -f2 | awk '{print $4}')
                if [ -z "${temp}" ]
                then
                   temp=$(sudo ipmitool sensor get_temp_reading "Front Panel" 2> /dev/null| grep "Sensor Reading" | cut -d"|" -f2 | awk '{print $4}')
                fi
                echo ${temp}
                ;;

'usuarios')
                IPCLUSTER=`( sudo /bin/grep ltsp /etc/dhcp/dhcpd.conf 2> /dev/null || sudo /bin/grep ltsp /etc/dhcpd.conf 2> /dev/null ) | head -1 | cut -d: -f1 | cut -d\" -f2`
                IP=`/sbin/ip a | grep -c $IPCLUSTER`
                if [ `echo $IP` -eq 1 ]
                  then
                    sudo /usr/bin/shell/userconectado | grep -v root | wc -l
                  else
                    echo 99
                fi
;;

'monit')
        sudo monit status | grep -B 1 "not monitored" | grep Process | cut -d\' -f2 | wc -l
;;

'duplex')
        sudo ethtool eth0 | grep "Duplex" | cut -d: -f2 | cut -d" " -f2
;;

'p2k')
        sudo grep pserver /etc/hosts | wc -l
;;

'p2k_close_23')
        unset LANG
        sudo netstat -anp | grep :4023 | grep CLOSE_WAIT | wc -l
;;

'p2k_close_68')
        unset LANG
        sudo netstat -anp | grep :4068 | grep CLOSE_WAIT | wc -l
;;

'eth0_ip')
        IP=`sudo cat /etc/sysconfig/network-scripts/ifcfg-eth0 | grep IPADDR | cut -d= -f2`
        ifconfig eth0 | grep -c $IP
;;

'speed_eth0')
        SPEED=`sudo ethtool eth0 | grep Speed | cut -d" " -f2 | cut -dM -f1`
        if [ `echo $SPEED` = Unknown! ]
                then
                        echo 0
                else
                        echo $SPEED
        fi
;;

'eth1_ip')
        IP=`sudo cat /etc/sysconfig/network-scripts/ifcfg-eth1 | grep IPADDR | cut -d= -f2`
        ifconfig eth1 | grep -c $IP
;;

'speed_eth1')
        SPEED=`sudo ethtool eth1 | grep Speed | cut -d" " -f2 | cut -dM -f1`
        if [ `echo $SPEED` = Unknown! ]
                then
                        echo 0
                else
                        echo $SPEED
        fi
;;

'home')
        /bin/df | /bin/grep /home/user | wc -l
        ;;
'atd')
        sudo /usr/bin/tail -50 /var/log/messages | grep -c "is in wrong format - aborting"
        ;;
'srv_ip')
        /sbin/ifconfig  |grep "inet end" |grep -v 127.0.0.1
        ;;
'drbd')
        cat /proc/drbd | grep "cs:"
        #/etc/init.d/drbd status | grep -A 1 "cs"
        #/etc/init.d/drbd status |grep "cs:"
        ;;
'vm')
OPERADOR=4577
SENHA=4577

LOJA=$(uname -n)

SITE=${LOJA}.lojasrenner.com.br
HTTP="https://"
PORT="8443"
WS="linx-webservices"
API="signin"
URL=${HTTP}${SITE}:${PORT}/${WS}/${API}

SHA512=$(echo -n ${URL} | openssl dgst -sha512 -hmac "RSP12345AZ" | cut -d"=" -f2)

teste=$(curl -s POST ${URL} \
    -H "accept: application/json" \
    -H "Application-Name: renner-selfpayment" \
    -H "Request-Key: ${SHA512}" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "username=${OPERADOR}&password=${SENHA}")

echo "${teste}" | egrep '*4023*' > /dev/null

if [ $? -eq 0 ]
then
    echo 0
else
    echo 1
fi

;;

'io')
        case $3 in
        'read.ops')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$4}"
                ;;
        'read.ms')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$7}"
                ;;
        'write.ops')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$8}"
                ;;
        'write.ms')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$11}"
                ;;
        'io.active')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$12}"
                ;;
        'io.ms')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$13}"
                ;;
        'read.sectors')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$6}"
                ;;
        'write.sectors')
                cat /proc/diskstats | grep $2 | head -1 | awk "{print $$10}"
                ;;
        *)
                echo "-- Statisticass de IO --"
                echo "Funcoes Validas:"
                echo "  Threads          -- Numero de threads do MySQL."
                ;;
        esac
        ;;
'mysql')
        case $2 in
        'help')
                echo "-- Status do MySQL --"
                echo "Funcoes Validas:"
                echo "  Threads          -- Numero de threads do MySQL."
                echo "  Queries          -- Total de queries executadas."
                echo "  Slow_queries     -- Total de Slow Queries."
                echo "  Opens            -- Opens."
                echo "  Open_tables      -- Status do MySQL."
                echo "  Qps              -- Versao do script."
                echo "  Version          -- Versao do script."
                echo "  Uptime           -- Versao do script."
                echo "  Sec_BM           -- Segundos em atras."
                echo "  Slave_statusfull -- Full Status do Slave"
                echo "  Slave_status     -- Status do IO e do SQL."
                echo "  Ping             -- Verifica se o MySQL esta ativo."
                echo "  help             -- Funcoes disponiveis."
                ;;
        'help')
                ./$0 mysql help
                ;;
        'Slave_statusfull')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "show slave status\G"
                ;;
        'Slave_status')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "show slave status\G" |grep Slave_
                ;;
        'Sec_BM')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "show slave status\G" |grep Seconds_Behind_Master
                ;;
        'Uptime')
                /u/mysql/bin/mysqladmin -updv -ppdv1997 status | awk {'print $2'}
                ;;
        'Threads')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "status;"|grep Threads |awk {'print $2'}
                ;;
        'Queries')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "status;"|grep Threads |awk {'print $4'}
                ;;
        'Slow_queries')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "status;"|grep Threads |awk {'print $7'}
                ;;
        'Opens')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "status;"|grep Threads |awk {'print $9'}
                ;;
        'Open_tables')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "status;"|grep Threads |awk {'print $15'}
                ;;
        'Qps')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "status;"|grep Threads |awk {'print $20'}
                ;;
        'Version')
                /u/mysql/bin/mysql -h `uname -n` -u pdv -ppdv1997 -Bse "status;" |grep "Server version" |awk {'print $3'}
                ;;
        'Ping')
                /u/mysql/bin/mysqladmin -u pdv -ppdv1997 ping
                ;;
        *)
                ./$0 mysql help
                ;;
        esac
        ;;
'nis')
        yppoll -h $2 -d $3 passwd.byname
        ;;
'md_status')
        cat /proc/mdstat
        ;;
'rpcinfo')
        rpcinfo -p $2
        ;;
'clarity')
        /niku/clarity/bin/niku status all
        ;;
'lock_siv')
                ( md5sum /tmp/$2 || echo $$ ) | awk {'print $1'}

        #echo ${ARQUIVOS}
        ;;

'arquivos_siv')
        ARQUIVOS=`ls /work/imagens/*tiff 2>/dev/null |wc -l`
        if [ ${ARQUIVOS} -gt 0 ]
        then
            echo 1
        else
            echo 0
        fi
        #echo ${ARQUIVOS}
        ;;
'caunicenter')
        /u/CA/UnicenterNSM/bin/unifstat
        ;;
'cat52')
        cd /u/pdv/linux/RFD/
        for i in `ls ZP2*.* IPB.*`
        do
                FILES=${FILES}"$i "
        done
        SERV=`hostname -s`
        VALUE=`echo ${FILES} |sed 's/ $//g'`
        /usr/local/sbin/zabbix_sender -z sp01 -s${SERV} -kcat52 -o"${VALUE}" >> /dev/null
        echo ${FILES} |sed 's/ $//g'
        ;;
'cups')
        case $2 in
        'devices')
                lpstat -v
                ;;
        *)
                lpstat -p
                ;;
        esac
        ;;

#==== Monitoramento servidores ====
'servidor')
        case $2 in
        'MARCA')
                 if [ `ls -l /etc/redhat-release | wc -l` = "1" ]
                    then
                       lshal | grep system.firmware.vendor | cut -d\' -f2
                    else
                       hwinfo --bios | grep "Manufacturer:" | cut -d'"' -f2 | head -1
                 fi
                ;;
        'MODELO')
                 if [ `cat /etc/redhat-release | grep -c "release 6"` = "1" ]
                    then
                       lshal | grep system.hardware.product | cut -d\' -f2
                    else
                       sudo lshw | grep "product:" | cut -d':' -f2 | cut -d"(" -f1 | sed 's/^ //g' | head -1
                 fi
                ;;

        'DATA')
                 /usr/bin/sudo /usr/sbin/lshw | grep date: | cut -d: -f2 | cut -d" " -f2
                ;;
        'SO')
                 if [ `ls -l /etc/redhat-release | wc -l` = "1" ]
                    then
                       cat /etc/redhat-release
                    else
                       cat /etc/SuSE-release | head -1
                 fi
                ;;
        'PROCESSADOR')
                 cat /proc/cpuinfo | grep "model name" | cut -d: -f2 | head -1
                ;;
        'CPU_USO')
                 top -bn1 | grep "Cpu(s)" | cut -d: -f2 | cut -d, -f1
                ;;
        'MEMORIA')
                 cat /proc/meminfo | grep MemTotal: | cut -d: -f2 | awk '{print $1}'
                ;;
        'MEMORIA_USO')
                 cat /proc/meminfo | grep MemFree:
                ;;
        'DISCO')
                 df -h | head -3 | tail -1 | awk '{print $1}'
                ;;
        'DISCO_TOTAL')
                  sudo fdisk -l /dev/sda | grep "Disk /dev/sda" | cut -d"," -f2 | cut -d" " -f2
                ;;
        'DISCO_USO')
                 df -h | head -3 | tail -1 | awk '{print $2}'
                ;;
        *)
                echo ""
                ;;
        esac
        ;;

#==== baseline seguranca lojas -- INICIO




'check_baseline_seg_lojas')

RETORNO_BASELINE=""
SUSE_RELEASE=`cat /etc/SuSE-release | grep VERSION | cut -d= -f2`

if [ $SUSE_RELEASE -eq  9 ]
then
        if [ `find /etc/lilo.conf -perm -600 | wc -l` -eq 1 ]
        then
                        RETORNO_BASELINE=${RETORNO_BASELINE}"lilo:1|"
        else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"lilo:0|"
        fi
else
     RETORNO_BASELINE=${RETORNO_BASELINE}"lilo:1|"
fi

        unset LANG
        _IP=`/sbin/ifconfig eth0 | grep "inet addr" | cut -d: -f2 | awk ' { print $1}' `

        if [ ${#_IP} -ne 0 ]
        then
            _RANGE=`/sbin/ifconfig eth0 | grep "inet addr" | cut -d: -f2 | awk ' { print $1}'|cut -d. -f1,2,3`
        else
           _IP=`/sbin/ifconfig eth1 | grep "inet addr" | cut -d: -f2 | awk ' { print $1}' `
           _RANGE=`/sbin/ifconfig eth1 | grep "inet addr" | cut -d: -f2 | awk ' { print $1}'|cut -d. -f1,2,3`
        fi

        if [ ${#_IP} -eq 0 ]
        then
            echo "ERRO: Nao Consegui setar IP ... abortando !!! "
            exit
        fi
                _OPT1="(rw,no_root_squash,sync)"
                _OPT2="(ro,no_root_squash,sync)"
                _OPT3="(rw,sync)"
                _OPT4="(rw,no_root_squash,sync)"
                v1=0 ; v2=0 ; v3=0 ; v4=0 ; v5=0 ; v=0 ; vx=0

        NDIR=`grep -c -e '^/' /etc/exports `
        while read line
        do

                DIR=`echo $line| grep -e '^/' | awk '{ print $1}' `

                if [ -n "$DIR" ]
                then

                        case $DIR in
                        "/u/pdv/linux")
                                v1=`grep "$DIR " /etc/exports | grep -c "$_RANGE".0/255.255.255.0 `
                                ;;

                        "/opt/ltsp")
                                v2=`grep "$DIR " /etc/exports | grep -c "$_RANGE".0/255.255.255.0 `
                                ;;

                        "/var/opt/ltsp/swapfiles")
                                v3=`grep "$DIR " /etc/exports | grep -c "$_RANGE".0/255.255.255.0 `
                                ;;

                        "/DRDB")
                                v4=`grep "$DIR " /etc/exports | grep -c "$_RANGE".0/255.255.255.0 `
                                ;;

                        *)
                                vx=`grep "$DIR " /etc/exports | grep -c "$_RANGE".0/255.255.255.0`
                                v5=`expr $v5 + $vx `
                                ;;
                        esac
                fi

        done < /etc/exports

                v=$(( $v1 + $v2 + $v3 + $v4 + $v5 ))

                if [ $v -ne $NDIR ]
                then
                        RETORNO_BASELINE=${RETORNO_BASELINE}"exports:0|"
                else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"exports:1|"
                fi

#'check_ftp')
#if [ $SUSE_RELEASE -eq  9 ]
if [ -f /etc/xinetd.d/vsftpd ] # Para SLES 11
then


                _IFTP=`ps axuw | grep -v grep | grep -v tftpd | grep -ci ftpd `
                if [ $_IFTP -eq 0 ]
                then
                        if [ `sudo /usr/bin/grep  -v ^# /etc/xinetd.d/vsftpd | grep disable | grep -c yes` -eq 1 ]
                        then
                             RETORNO_BASELINE=${RETORNO_BASELINE}"ftp:1|"
                        else
                             RETORNO_BASELINE=${RETORNO_BASELINE}"ftp:0|"
                        fi
                else
                   RETORNO_BASELINE=${RETORNO_BASELINE}"ftp:0|"
                fi
else
    RETORNO_BASELINE=${RETORNO_BASELINE}"ftp:1|"
fi

#'check_snmp')
if [ $SUSE_RELEASE -eq  9 ]
then
                _ISNMP=`ps axuw | grep -v grep | grep -ci snmpd `
                if [ $_ISNMP -eq 0 ]
                then
                        if [ `/sbin/chkconfig snmpd --list | grep -ci on ` -eq 0 ]
                        then
                               RETORNO_BASELINE=${RETORNO_BASELINE}"snmp:1|"
                        else
                              RETORNO_BASELINE=${RETORNO_BASELINE}"snmp:0|"
                        fi
                else
                       RETORNO_BASELINE=${RETORNO_BASELINE}"snmp:0|"
                fi
else
    RETORNO_BASELINE=${RETORNO_BASELINE}"snmp:1|"
fi


#'check_resolv_conf')

                        V1=`grep -c "search lojasrenner.com.br" /etc/resolv.conf`
                        V2=`grep -c "nameserver 10.1.4.100" /etc/resolv.conf`
                        V3=`grep -c "nameserver 10.20.4.100" /etc/resolv.conf`
                        if [ $V1 -eq 0 -a $V2 -eq 0 -a $V3 -eq 0 ]
                        then
                           RETORNO_BASELINE=${RETORNO_BASELINE}"resolv:0|"
                        else
                           RETORNO_BASELINE=${RETORNO_BASELINE}"resolv:1|"
                        fi

#'check_aliases')
                echo "mailer-daemon:  root
                administrator:  root
                daemon:         root
                lp:             root
                at:             root
                msql:           root
                nobody:         root
                bin:            root
                hostmaster:     root
                mail:           root
                webmaster:      root" > /tmp/aliases_check

                diff -w /etc/aliases /tmp/aliases_check
                if [ $? -eq 0 ]
                then
                   RETORNO_BASELINE=${RETORNO_BASELINE}"aliases:1|"
                else
                   RETORNO_BASELINE=${RETORNO_BASELINE}"aliases:0|"
                fi


#'check_umask')

                if [ `grep -c "else umask 077" /etc/profile ` -eq 0 ]
                then
                     RETORNO_BASELINE=${RETORNO_BASELINE}"umask:0|"
                else
                    RETORNO_BASELINE=${RETORNO_BASELINE}"umask:1|"
                fi

#'check_sysctl')

                V1=`grep "net.ipv4.conf.all.accept_redirects" /etc/sysctl.conf  | grep -c 0`
                V2=`grep "net.ipv4.conf.all.forwarding" /etc/sysctl.conf | grep -c 0`

                if [ $V1 -eq 0 -o $V2 -eq 0 ]
                then
                     RETORNO_BASELINE=${RETORNO_BASELINE}"sysctl:0|"
                else
                     RETORNO_BASELINE=${RETORNO_BASELINE}"sysctl:1|"

                fi


#'check_root_ponto')

                if [ `grep -v ^# /etc/profile.renner | grep PATH | grep shell | grep -c "\."` -eq 0 ]
                then
                        RETORNO_BASELINE=${RETORNO_BASELINE}"root_ponto:1|"
                else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"root_ponto:0|"
                fi


#'check_chmod_tmp')

                if [ `ls -ld /tmp| grep -c "drwxrwxrwt"` -eq 0 -o `ls -ld /var/tmp| grep -c "drwxrwxrwt"` -eq 0 ]
                then
                        RETORNO_BASELINE=${RETORNO_BASELINE}"chmod_tmp:0|"
                else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"chmod_tmp:1|"
                fi


#'check_issue')

                if [ `sudo /usr/bin/grep -c "AUTORIZADOS" /etc/issue` -eq 0 ]
                then
                        RETORNO_BASELINE=${RETORNO_BASELINE}"issue:0|"
                else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"issue:1|"
                fi


#'check_motd')

                if [ -s /etc/motd ]
                then
                        RETORNO_BASELINE=${RETORNO_BASELINE}"motd:0|"
                else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"motd:1|"
                fi


#'check_smb')

                _LOJA=`uname -n| cut -c1`
                ####if [ $_LOJA = "h" ]
                if [ $_LOJA = "L" ]
                then

                        smb1=`grep -i "null Passwords" /etc/samba/smb.conf | grep -ci  no`
                        smb2=`grep -i "encrypt passwords" /etc/samba/smb.conf | grep -ci  yes`
                        smb3=`grep -i "valid users" /etc/samba/smb.conf | grep -ci  sivimg`
                        if [ $smb1 -eq 0 -o $smb2 -eq 0 -o $smb3 -eq 0 ]
                        then
                                RETORNO_BASELINE=${RETORNO_BASELINE}"smb:0|"
                        else
                                RETORNO_BASELINE=${RETORNO_BASELINE}"smb:1|"
                        fi
                else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"smb:1|"
                fi

#'check_sshd_config')

                v=0 ; v1=0 ; v2=0 ; v3=0 ; v4=0 ; v5=0 ; v6=0 ; v7=0 ; v8=0 ; v9=0 ; v0=0 ;
                v1=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep Banner | grep -c /etc/issue`
                v2=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep PermitEmptyPasswords | grep -c no`
                v3=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep IgnoreRhosts | grep -c yes `
                v4=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep RhostsRSAAuthentication | grep -c no `
                v5=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep Protocol | grep -c 2 `
                v6=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep StrictModes | grep -c yes `
                v7=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep HostbasedAuthentication | grep -c yes`
                v8=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep PermitUserEnvironment | grep -c no`
                v9=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep LoginGraceTime | grep -c 45`
                v0=`sudo /usr/bin/grep -v ^# /etc/ssh/sshd_config | grep KeyRegenerationInterval| grep -c 1800 `

                v=$(( $v1 + $v2 + $v3 + $v4 + $v5 + $v6 + $v7 + $v8 + $v9 + $v0 ))

                #if [ $v -ne 10 ]
                if [ $v -ge 10 ]
                then
                        RETORNO_BASELINE=${RETORNO_BASELINE}"sshd_config:1|"
                else
                        RETORNO_BASELINE=${RETORNO_BASELINE}"sshd_config:0|"
                fi

#'check_400_ssh')
                RET_CHMOD=1
               # for i in ` sudo find /  -name "\.ssh" -type d 2> /dev/null  `

                DIR_SSH=` sudo find /.ssh   -name "\.ssh" -type d  ;sudo find /home/user/*/.ssh -name "\.ssh" -type d  ;sudo find /u/mysql/.ssh  -name "\.ssh" -type d   ;sudo find  /u/ora10g/.ssh -name "\.ssh" -type d  ` 2>/dev/null
                #echo $DIR_SSH
                for i in  $DIR_SSH
                do


                   for j in ` sudo  find $i -type f `
                   do
                      V=`sudo ls -l $j  | grep -v gvfs | grep -v listing | grep -v known_hosts | grep -v -c  -e "-r--------" `
                      if [ $V -gt 0 ]
                      then
                          RET_CHMOD=0
                          break 2
                      fi
                   done
                done



                if [ $RET_CHMOD -eq 0 ]
                then
                    RETORNO_BASELINE=${RETORNO_BASELINE}"chmod_ssh:0"
                else
                    RETORNO_BASELINE=${RETORNO_BASELINE}"chmod_ssh:1"
                fi


echo ${RETORNO_BASELINE}


        ;;
#==== baseline seguranca lojas -- FIM

'check_arq_cpf')

cd /work/pdfimg

TESTE_CPF=`ls cpf_* 2>/dev/null `

# Verifica se tem arquivos para transferir
if test  -z "${TESTE_CPF}"
then
    echo 1
    exit
fi

for i in `ls cpf*`
do
        if [ `echo ${i} | egrep -c cpf_[0-9]{11}_[0-9]{2}_[0-9]{17}.pdf ` != 1 ]
        then
                echo 0
                exit
        fi
done
echo 1
      ;;

'yum_check_update')
        if [ `/usr/bin/yum --security check-update | grep -c  "No packages needed for security" ` -eq 1 ]
        then
                echo "1"
        else
                echo "0"
        fi

        ;;

'help')
        echo "monitor version: $version"
        echo "Uso:"
        echo "    $0 cluster                    -- Verifica se o IP do clustes esta ativo."
        echo "    $0 cups                       -- Status das impressoras (cups)."
        echo "    $0 cat52                      -- Arquivos do CAT52"
        echo "    $0 caunicenter                -- Status do CA Unicenter"
        echo "    $0 clarity                    -- Status do Clarity"
        echo "    $0 drbd                       -- Status do drbd."
        echo "    $0 mysql <help>               -- Status do MySQL"
        echo "    $0 md_status                  -- Status do RAID (MD)"
        echo "    $0 rpcinfo <help>             -- Status do RPC"
        echo "    $0 versao                     -- Versao do script."
        echo "    $0 area_bco_mysql             -- Verifica area do banco MySQL."
        echo "    $0 offline_pdv                -- Verifica carga de vendas (offline)."
        echo "    $0 nis <host> <domain>        -- Disponibilidade do NIS."
        echo "    $0 check_baseline_seg_lojas   -- Verifica aplicacao baseline seguranca lojas."
        echo "    $0 yum_check_update           -- Verifica se necessita aplicar algum patch de seguranç(rpm)."
        echo "    $0 help                       -- Funcoes disponiveis."
        rval=1
        exit $rval
        ;;
*)      echo "ZBX_NOTSUPPORTED"
        ;;
esac

rval=$?

exit $rval
