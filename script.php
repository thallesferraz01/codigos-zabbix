<?php

$database_host = 'localhost';
$database_name = 'NAME';
$database_table = 'NAME';
$database_username='root';
$database_password = 'PASS';
// Connect to the database
$dbc = mysqli_connect($database_host, $database_username,
$database_password, $database_name);

// Executa o script disponibilidade_servidores_lojas.py
#$output = shell_exec('/usr/bin/python /etc/zabbix/scripts/disponibilidade/disponibilidade_servidores_lojas.py > servidores2.txt');

// Set the charset to utf8
mysqli_set_charset($dbc,"utf8");

// Read the data
$handle = fopen("/tmp/ARQUIVO", "r"); //NOME ARQUIVO PARA LER

// Set $i = 0 for further usage
$i=0;

// Truncate Table b4 insert new data
$trunk = 'TRUNCATE table NOMETABLE'; //table
mysqli_query($dbc, $trunk);


// Use fgetcsv function along with while loop to get all of the rows in the file
while (($data = fgetcsv($handle, 1000, ';')))
{

     // Since the first line in the file is column name,
     // so we are going to skip the first line.
     // When $i = 0, it should be in the first line,
     // it gets into the if function,
     // and the continue skip all the codes afterwards
     // and get back to the top of the loop,
     // and in this round the $i = 1. So it will not get into if function,
     // only skip the first line that we don't want.


    if($i == 0)
    {
        $i++;
        continue;
    }


    // Finally, we insert the data into our database.
    $query = 'INSERT INTO NOMETABLE (host,alertas,qtd,time,date)
VALUES ("'.$data[0].'","'.$data[1].'","'.$data[2].'","'.$data[3].'", CURDATE()-1)';

   // echo $query;
    $result = mysqli_query($dbc, $query);

    if ($result == false)
    {
        echo 'Error description <b r/>' . mysqli_error($dbc);
    }
}
?>

