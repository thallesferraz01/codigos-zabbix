#! /bin/bash

echo '{'
echo ' "data":['
index=`snmpbulkwalk -v 2c -On -m '' -c varejo ${1} 1.3.6.1.4.1.14179.2.2.1.1.3 | grep ${2} | awk -F '1.3.6.1.4.1.14179.2.2.1.1.3.' '{print $2}' | cut -d" "  -f1`
snmpbulkwalk -v 2c -On -m '' -c varejo ${1} 1.3.6.1.4.1.9.9.517.1.2.2.1.1.${index} | awk -F'1.3.6.1.4.1.9.9.517.1.2.2.1.1.'${index} '{print $2}' | while read Line; do
  indexID=`echo ${Line} | cut -d" "  -f1`
  snmpbulkwalk -v 2c -On -m '' -c varejo ${1} 1.3.6.1.4.1.9.9.512.1.1.1.1.4${indexID} | awk -F'1.3.6.1.4.1.9.9.512.1.1.1.1.4'${indexID} '{print $2}' | while read name; do
    profileName=`echo $name | cut -d" " -f3 | sed 's/\"//g' | sed 's/^\.//g'`
    Interface=`grep ${Index} /tmp/interface_vlan | cut -d: -f2 | cut -d" " -f2`

    echo -n '  {"{#HOST}":"'${2}'", "{#INDEXAP}":"'${index}'", "{#INDEXID}":"'${indexID}'", "{#PROFILE}":"'${profileName}'", "{#MACRO}":"'\{\$REAP_${profileName}\}'"},'


  done
done | sed 's/,$//' | sed 's/},/},\n/g'
echo
echo ' ]'
echo '}'
