#!/bin/bash
#Monitoramento de inventario de DB

fn_ora()
{
umask "0000"
export ORACLE_HOME=/usr/lib/oracle/11.2/client64/
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

DB_HOST=$1
DB_SID="SERVICE_NAME=$2";
DB_USER=$3;
DB_PW=$4;
DB_PORT="1521";


$ORACLE_HOME/bin/sqlplus -s "$DB_USER/$DB_PW@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = $DB_HOST)(PORT = $DB_PORT))(CONNECT_DATA = ($DB_SID)))"

}

fn_ora DB1929 md08 dbsnmp 'DBSNMPtivitrenner' <<EOF > /etc/zabbix/coletas_db/coleta_mid.out

set echo off
set feedback off
set heading off
set trimspool on
set lines 2000

delete from obs_mid where managed_servers = '';

select  ddb.TARGET_NAME||';'||db.target_type||';'||db.type_qualifier2||';'||db.DISPLAY_NAME||';'||db.host_name||';'||db.TYPE_DISPLAY_NAME||';'||db.creation_date||';'||os.TYPE_QUALIFIER1||';'||os.TYPE_QUALIFIER2  from SYSMAN.MGMT\$TARGET db, SYSMAN.MGMT\$TARGET os where db.HOST_NAME = os.TARGET_NAME and db.TARGET_TYPE not in ('oracle_database','rac_database','cluster','oracle_pdb','oracle_home', 'oracle_listener','has','oracle_si_netswitch','oracle_exa_cisco_switch','oracle_sdpmessagingdriver_email','oracle_sdpmessagingdriver','oracle_sdpmessagingdriver_xmpp','oracle_irm','oracle_exa_pdu','oracle_analytics','oracle_exa_ilom','oracle_dblra','oracle_si_server_map','oracle_sdpmessagingserver','oracle_sdpmessagingdriver_smpp','oracle_coherence_cache','oracle_personalization','oracle_repapp','metadata_repository','microsoft_sqlserver_database','oracle_odi_standaloneagent','oracle_ibswitch','oracle_ibnetwork','oracle_exadata','oracle_dbmachine','oracle_ucm', 'oracle_activitygraph','osm_proxy','oracle_beacon','oracle_reptools','oracle_oms_pbs', 'host','bea_alsb','oracle_emd','oracle_forms','oracle_emrep','oracle_repserv');


EOF
