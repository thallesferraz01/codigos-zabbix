ZABBIX_SENDER="/usr/bin/zabbix_sender";
LOG=/etc/zabbix/scripts/snmp.logs

read hostname
read ip
read uptime
read oid
read address
read community
read enterprise
read v1
read v2
read v3
read v4
read v5
read v6
read v7
read v8
read v9
read v10
read v11
read v12
read v13
read v14
read v15

echo "-------------------INICIO------------------"  >> $LOG
echo "hostname => $hostname" >> $LOG
echo "ip => $IP" >> $LOG
echo "uptime => $uptime" >> $LOG
echo "oid => $oid" >> $LOG
echo "address => $address" >> $LOG
echo "community => $community" >> $LOG
echo "enterprise => $enterprise" >> $LOG
echo "v1 => $v1" >> $LOG
echo "v2 => $v2" >> $LOG
echo "v3 => $v3" >> $LOG
echo "v4 => $v4" >> $LOG
echo "v5 => $v5" >> $LOG
echo "v6 => $v6" >> $LOG
echo "v7 => $v7 " >> $LOG
echo "v8 => $v8 " >> $LOG
echo "v9 => $v9 " >> $LOG
echo "v10 => $v10 " >> $LOG
echo "v11 => $v11 " >> $LOG
echo "v12 => $v12 " >> $LOG
echo "v13 => $v13 " >> $LOG
echo "v14 => $v14 " >> $LOG
echo "v15 => $v15 " >> $LOG

echo "-----------------FIM--------------------"  >> $LOG


ALARME=$(echo $v1 |awk '{print $4}' |sed  's/"//g')
MSG=$(echo $v2 |awk '{print $4}' |sed  's/"//g')
HOST=$(echo $v4 |awk '{print $4}' |sed  's/"//g')

case $ALARME in
alarm.DatastoreDiskUsageAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.HAinsufficientFailoverResources) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.HostCPUUsageAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.HostConnectionStateAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.HostConnectivityAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.HostMemoryUsageAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.NetworkConnectivityLostAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.NetworkRedundancyDegradedAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.NetworkRedundancyLostAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.StorageConnectivityAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.StorageHealthAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.VmDiskConsolidationNeededAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.VsanHostDiskErrorAlarm) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
alarm.vsan.health.test.overallsummary) $ZABBIX_SENDER -z SP89   -p 10051 -s $HOST -k $ALARME -o "$MSG";;
esac
