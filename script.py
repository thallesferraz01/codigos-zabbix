#!/usr/bin/python
# -*- coding: utf-8 -*-

from zabbix_api import ZabbixAPI
import os
import datetime as dt
from time import mktime
import multiprocessing as Process

zapi = ZabbixAPI(server='https://URL-ZABBIX', path="", log_level=0, timeout=300)
zapi.login('USERAPI', 'PASSWORD')

def sumOfList(list, size):
   if (size == 0):
     return 0
   else:
     return list[size - 1] + sumOfList(list, size - 1)

def get_hostid(host):
    hostId = zapi.host.get({"output":"hostid","filter":{"host":host}})[0]['hostid']
    return hostId

def get_event(triggerId,val,countOut):
    today = dt.datetime.now()
    previous_day = today - dt.timedelta(days=280)
    today_morning = previous_day.strftime('%Y/%m/%d') + " 00:00:00"
    today_evening = today.strftime('%Y/%m/%d') + " 23:59:59"
    datetime_ini = dt.datetime.strptime(today_morning, '%Y/%m/%d %H:%M:%S')
    datetime_fim = dt.datetime.strptime(today_evening, '%Y/%m/%d %H:%M:%S')

    Date_ini = int(datetime_ini.strftime("%s"))
    Date_fim = int(datetime_fim.strftime("%s"))
    tidcount = zapi.event.get({"output":"extend","objectids":triggerId,"time_from":Date_ini,"time_till":Date_fim,"value": val,"sortorder":  "desc"})
    return tidcount


def findEvent(array, eventSearch):
        for value in array:
            if (value["eventid"] == eventSearch):
                return value["clock"];

def get_triggers_hosts(host):
    triggers = zapi.trigger.get({"hostids":get_hostid(host),"expandDescription":"true","status": "0","state": "0","expandComment":"true","expandExpression":"true"})
    for x in triggers:
        if ("TRIGGER NAME" in x['description']): # COLOCAR TRIGGER
         #if (("MACRO" not in x['description'])  and ("Cluster" not in x['description']) and ("não" not in x['description'])):
                linkProblem = get_event(x['triggerid'], "1", "false")
                linkRecovery = get_event(x['triggerid'], "0", "false")
                linkDuration = eventDuration(linkProblem,linkRecovery)
                #print "########################################################"
                print host+";"+x['description']+";"+get_trigger_count(x['triggerid'],"1","true")+";"+str(linkDuration)
                #print "########################################################"

def get_trigger_count(triggerId,val,countOut):
    today = dt.datetime.now()
    previous_day = today - dt.timedelta(days=7)
    today_morning = previous_day.strftime('%Y/%m/%d') + " 00:00:00"
    today_evening = today.strftime('%Y/%m/%d') + " 23:59:59"
    datetime_ini = dt.datetime.strptime(today_morning, '%Y/%m/%d %H:%M:%S')
    datetime_fim = dt.datetime.strptime(today_evening, '%Y/%m/%d %H:%M:%S')

    Date_ini = int(datetime_ini.strftime("%s"))
    Date_fim = int(datetime_fim.strftime("%s"))
    #print triggerId

    tidcount = zapi.event.get({"output":"objectids","objectids":triggerId,"time_from":Date_ini,"time_till":Date_fim,"value": val,"sortorder":  "desc","countOutput": countOut})
    return tidcount



def eventDuration(arrayProblem,arrayRecovery):
        total = []
        total_qtd = []
        count = 0
        problema = 1
        ultimo = 0
        tempo_recovery = 0
        tempo_problema = 0
     #   print arrayProblem
     #   print arrayRecovery

        tamanho_recuperacao=len(arrayRecovery)
        tamanho_problema=len(arrayProblem)

        arrayTotal = arrayProblem + arrayRecovery
        arrayTotal.sort(key=lambda d: d['clock'])
        tamanho_total=len(arrayTotal)
        #print arrayTotal

        for value in arrayTotal:
            #print value["clock"]
            #print "Tipo de evento"
            #print value["value"]

            if int(value["value"]) == 1:
               tempo_problema = int(value["clock"])
              # print "Problema"

            if int(value["value"]) == 0:
               tempo_recovery = int(value["clock"])
              # print "Recovery"

            if ultimo+1 == tamanho_total and int(value["value"]) == 1:
               today = dt.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
               datetime_ini = dt.datetime.strptime(str(today), '%Y/%m/%d %H:%M:%S')
               tempo_recovery = int(datetime_ini.strftime("%s"))

            if tempo_recovery <> 0 and tempo_problema <> 0:
               tot = tempo_recovery - tempo_problema
               total_qtd.append(tot)

            #print "Tempo de Problema"
            #print tempo_problema
            #print "Tempo de Recovery"
            #print tempo_recovery
            #print "Tempo Total"
            #print sum(total_qtd)
            #print "################################"
            ultimo = ultimo + 1
            tempo_recovery = 0
        #print "Tempo Total Final"
        #print sum(total_qtd)

        total_array = tamanho_recuperacao + tamanho_problema

        for value in arrayProblem:
            dateProblem = value["clock"]
            if count+1 <= tamanho_recuperacao:
                dateRecovery = findEvent(arrayRecovery,arrayRecovery[count]["eventid"])
            else:
                today = dt.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
                datetime_ini = dt.datetime.strptime(str(today), '%Y/%m/%d %H:%M:%S')
                dateRecovery = int(datetime_ini.strftime("%s"))
            if (dateProblem > dateRecovery) and (tamanho_problema > 0):
                today = dt.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
                datetime_ini = dt.datetime.strptime(str(today), '%Y/%m/%d %H:%M:%S')
                dateRecovery = int(datetime_ini.strftime("%s"))

            count = count + 1
            subTime = int(dateRecovery) - int(dateProblem)
            total.append(subTime)

        subTime = sum(total)

        y=subTime/(60*60*24*365)

        if (y < 1):
           y = "0"
        d = (subTime/(60*60*24))%365

        if (d < 1):
            d = "0"

        h = (subTime/(60*60))%24
        if (h < 1):
           h = "0"

        m = (subTime/60)%60

        if (m < 1):
            m = "0"

        #return str(y)+" years "+str(d)+" days "+str(h)+" hours "+str(m)+" minutes"
        #return subTime
        return sum(total_qtd)

Hosts = zapi.host.get({
    'output' : [ 'host' ],
    'selectInterfaces' : ['ip'],
    'monitored_hosts' : 1,
    'groupids' : 50250200000000539 #hostgroup
#    'groupids' : 50250200000000440
})

for Host in Hosts:
         get_triggers_hosts(Host['host'])
        # proc = Process(target=get_triggers_hosts, args=Host['host'])
        # proc.start()
        # time.sleep(0.5)


