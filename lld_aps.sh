#! /bin/bash

echo '['
snmpwalk -v 2c -On -m '' -c varejo ${1} 1.3.6.1.4.1.14179.2.2.1.1.3 | awk -F'.1.3.6.1.4.1.14179.2.2.1.1.3.' '{print $2}' | egrep "${2}" | while read line; do
    value=$(echo ${line} | cut -d" " -f4)
    index=$(echo ${line} | cut -d" " -f1)
    hostgroup=$(echo $value | cut -d\" -f2 | cut -d_ -f1)
    echo -n '  {"{#SNMPVALUE}":'${value}', "{#SNMPINDEX}":"'${index}'", "{#HOSTGROUP}":"'${hostgroup}'"}, '

done | sed 's/, $/ /' | sed 's/}, /},\n/g'
echo
echo ']'
