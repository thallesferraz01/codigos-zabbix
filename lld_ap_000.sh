#! /bin/bash

echo '{'
echo ' "data":['
snmpwalk -v 2c -On -m '' -c varejo 10.10.0.121 1.3.6.1.4.1.14179.2.2.1.1.3 | awk -F'.1.3.6.1.4.1.14179.2.2.1.1.3.' '{print $2}'| grep ADM | while read line; do
    value=$(echo ${line} | cut -d" " -f4)
    index=$(echo ${line} | cut -d" " -f1)
    echo -n '  { "{#SNMPVALUE}":'${value}', "{#SNMPINDEX}":"'${index}'" },'

done | sed 's/,$/ /' | sed 's/}, /},\n/g'

echo
echo ' ]'
echo '}'
