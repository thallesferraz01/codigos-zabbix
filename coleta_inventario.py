#!/usr/bin/python
# -*- coding: utf-8 -*-

from zabbix_api import ZabbixAPI
import os
import datetime as dt
from time import mktime
import multiprocessing as Process

zapi = ZabbixAPI(server='http://10.1.91.68/zabbix', path="", log_level=0, timeout=300)
zapi.login('apiuser', 'renner_2020')

def sumOfList(list, size):
   if (size == 0):
     return 0
   else:
     return list[size - 1] + sumOfList(list, size - 1)

def get_inventory():
    host = zapi.host.get({"output":["inventory", "name"], "groupids":"37", "withInventory": "1", "selectInventory": ["model"]})
    return host


for Inv in get_inventory():
    print Inv['inventory']['model']
